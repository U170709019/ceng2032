.data
str1: .asciiz "Enter an integer: "
str2: .asciiz "\nEnter another integer: "
str3: .asciiz "\nThe greatest common divisor is: "
.text
main:
la $a0,str1
addi $v0,$zero,4
syscall
addi $v0,$zero,5
syscall
addi $a1,$v0,0
la $a0,str2
addi $v0,$zero,4
syscall
addi $v0,$zero,5
syscall
addi $a2,$v0,0
slt $s0,$a2,$a1 
beq $s0,$zero,ayu
jal gcdloop
la $a0,str3
addi $v0,$zero,4
syscall
addi $a0,$a2,0
addi $v0,$zero,1
syscall
ayu:
addi $v0,$zero,10
syscall
gcdloop:
div $s0,$a1,$a2
mul $s0,$a2,$s0
beq $s0,$a1,yu
sub $a0,$a1,$s0
addi $a1,$a2,0
addi $a2,$a0,0
j gcdloop
yu:
jr $ra
