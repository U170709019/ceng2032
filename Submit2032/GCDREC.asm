.data
str1: .asciiz "Enter an integer: "
str2: .asciiz "\nEnter another integer: "
str3: .asciiz "\nThe greatest common divisor is: "
.text
main:
la $a0,str1
addi $v0,$zero,4
syscall
addi $v0,$zero,5
syscall
addi $a1,$v0,0
la $a0,str2
addi $v0,$zero,4
syscall
addi $v0,$zero,5
syscall
addi $a2,$v0,0
slt $s0,$a2,$a1 
jal gcdRec
la $a0,str3
addi $v0,$zero,4
syscall
addi $a0,$s7,0
addi $v0,$zero,1
syscall
ayu:
addi $v0,$zero,10
syscall
gcdRec:
bne $a2,$zero,dd
jr $ra
dd:
addi $sp,$sp,-12
sw $ra,0($sp)
sw $a1,4($sp)
sw $a2,8($sp)
div $s0,$a1,$a2
mul $s0,$a2,$s0
sub $a0,$a1,$s0
addi $a1,$a2,0
addi $a2,$a0,0
jal gcdRec
recursion:
lw $ra,0($sp)
bne $s7,$zero,gf
addi $s7,$a1,0
gf:
lw $a1,4($sp)
lw $a2,8($sp)
addi $sp,$sp,12
jr $ra
